Pod::Spec.new do |s|
	s.name             = 'pguzman2017'
	s.version          = '0.1.0'
	s.summary          = 'd07 piscine Swift'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

	s.description      = <<-DESC
day07 piscine Swift
                         DESC

	s.homepage         = 'https://github.com/daftMarc/mfamilar2017'
	s.license          = { :type => 'MIT', :file => 'LICENSE' }
	s.author           = { 'daftMarc' => 'marc.familari@gmail.com' }
	s.source           = { :git => 'https://github.com/daftMarc/mfamilar2017.git', :tag => s.version.to_s }
	s.ios.deployment_target = '8.0'
	s.source_files = 'pguzman2017/Classes/*'
	s.resources = 'pguzman2017/Classes/article.xcdatamodeld'

	s.frameworks = 'CoreData'
end
