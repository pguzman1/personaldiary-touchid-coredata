//
//  Article+CoreDataProperties.swift
//  Pods
//
//  Created by Patricio GUZMAN on 10/13/17.
//
//

import Foundation
import CoreData


extension Article {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Article> {
        return NSFetchRequest<Article>(entityName: "Article")
    }

    @NSManaged public var content: String?
    @NSManaged public var creation_date: NSDate?
    @NSManaged public var image: NSData?
    @NSManaged public var lang: String?
    @NSManaged public var modification_date: NSDate?
    @NSManaged public var title: String?

}
