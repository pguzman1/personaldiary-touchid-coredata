//
//  ViewController.swift
//  PersonnalDiary
//
//  Created by Patricio GUZMAN on 10/13/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit
import pguzman2017
import LocalAuthentication


let ad = UIApplication.shared.delegate as! AppDelegate
let context = ad.persistentContainer.viewContext
let myPassword = "ecole42"
var systemLang: String?

class ViewController: UIViewController {
    
    @IBOutlet weak var touchId: UIButton!
    @IBOutlet weak var iphoneCode: UITextField!
    @IBOutlet weak var enterButton: UIButton!

    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    var lacontext: LAContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.enterButton.setTitle(NSLocalizedString("enterKey", comment: "enterKey"), for: .normal)
        self.statusLabel.text = ""
        self.textLabel.text = NSLocalizedString("enterCodeMessage", comment: "Enter Code Message")
//        self.generateFakeArticles()
        self.lacontext = LAContext()
        self.showTouchId()
    }
    @IBAction func touchIdAgain(_ sender: UIButton) {
        self.showTouchId()
    }
    
    func showTouchId() {
        if lacontext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
            lacontext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Please verify yourself", reply: {(isYou, error) in
                DispatchQueue.main.async {
                    if isYou {
                        self.performSegue(withIdentifier: "FirstToSecond", sender: nil)
                    }
                }
            })
        }
        else {
            statusLabel.text = NSLocalizedString("TouchIdNoSupport", comment: "TouchId No Support")
            self.touchId.isHidden = true
        }
    }
    
    func generateFakeArticles() {
        let articleManager = ArticleManager(context: context)
        let article1 = articleManager.newArticle()
        article1.title = "FIRST ARTIClE"
        article1.creation_date = NSDate()
        article1.modification_date = NSDate()
        article1.content = "asdalskdhlaskjdhlaskdhaslkhdlsakdhlaskdjhaslkdjhaslkdjhaslkdjhaslkjdhaslkjdhsalkdkjdhalksdhlaskjdhaslkdjhaslkdjhaslkdjhaslkdjhaslkdhaslkdhaslkdhaslkdjhals"
        article1.lang = "en"
        article1.image = NSData(contentsOf: URL(string: "https://cdn.pixabay.com/photo/2016/06/18/17/42/image-1465348_960_720.jpg")!)
        articleManager.save()
    }
    
    @IBAction func clickEnter(_ sender: UIButton) {
        let password = iphoneCode.text!
        if password.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return
        }
        if password == myPassword {
            self.performSegue(withIdentifier: "FirstToSecond", sender: nil)
        }
        else {
            let alert = UIAlertController(title: "Wrong password", message: "try again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {(action) in
                alert.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}

