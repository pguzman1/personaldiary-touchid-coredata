//
//  FirstTableViewCell.swift
//  PersonnalDiary
//
//  Created by Patricio GUZMAN on 10/13/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit
import pguzman2017

class FirstTableViewCell: UITableViewCell {

    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var title: UILabel!
    var creationString = NSLocalizedString("creation", comment: "creation")
    var modificationString = NSLocalizedString("modification", comment: "modification")

    func setCell(article: Article) {
        if let imageData = article.image {
            myImage.image = UIImage(data: imageData as Data)
        }
        else {
            
        }
        if let details = article.creation_date {
            if let modif = article.modification_date {
                
                if details == modif {
                    self.details.text = creationString + ": " + details.toString(dateFormat: "dd-MM-yyyy HH:mm")
                }
                else {
                    self.details.text = creationString + ":" + details.toString(dateFormat: "dd-MM-yyyy HH:mm") + "\nModification: " + modif.toString(dateFormat: "dd-MM-yyyy HH:mm")
                }
            }
        }
        if let content = article.content {
            self.content.text = content
        }
        if let title = article.title {
            self.title.text = title
        }
    }
}

extension NSDate {
    
    //"yyyy-MM-dd"
    
    func toString(dateFormat format: String) -> String {
        let date = self
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let dateString = dateFormatter.string(from:date as Date)
        return dateString
    }
}
