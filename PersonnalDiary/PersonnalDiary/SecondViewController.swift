//
//  SecondViewController.swift
//  PersonnalDiary
//
//  Created by Patricio GUZMAN on 10/13/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit
import pguzman2017

class SecondViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    let articleManager = ArticleManager(context: context)
    var articles: [Article] = []
    var lang: String! = "en"
    var articleToEdit: Article?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("titleDiary", comment: "title of Diary")
        systemLang = Locale.preferredLanguages[0]
        articles = articleManager.getArticles(withLang: systemLang!)!
        table.estimatedRowHeight = 200
        table.rowHeight = UITableViewAutomaticDimension
    }
    
    
    @IBAction func addArticle(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "ArticlesToAdd", sender: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("will appear")
        articles = articleManager.getArticles(withLang: systemLang!)!
        table.reloadData()
    }
}

extension SecondViewController: UITableViewDelegate, UITableViewDataSource {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepare for segue")
        if segue.identifier == "ArticlesToEdit" {
            if let VC = segue.destination as? AddViewController {
                VC.articleToEdit = self.articleToEdit
                VC.title = NSLocalizedString("edit", comment: "edit")
            }
        }
        else {
            if let VC = segue.destination as? AddViewController {
                VC.title = NSLocalizedString("add", comment: "add")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "FirstTableViewCell") as! FirstTableViewCell
        cell.setCell(article: articles[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.articleToEdit = articles[indexPath.row]
        performSegue(withIdentifier: "ArticlesToEdit", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let selectedArticle = articles[indexPath.row]
            articleManager.removeArticle(article: selectedArticle)
            articles.remove(at: indexPath.row)
            table.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}
