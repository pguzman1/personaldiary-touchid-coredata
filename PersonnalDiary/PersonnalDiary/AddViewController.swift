//
//  AddViewController.swift
//  PersonnalDiary
//
//  Created by Patricio GUZMAN on 10/14/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit
import pguzman2017

class AddViewController: UIViewController {

    @IBOutlet weak var choosePictureButton: UIButton!
    @IBOutlet weak var takePictureButton: UIButton!
    @IBOutlet weak var mytitle: UITextField!
    @IBOutlet weak var text: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    var articleToEdit: Article?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        text.text = ""
        takePictureButton.setTitle(NSLocalizedString("takePicture", comment: "Take picure"), for: .normal)
        choosePictureButton.setTitle(NSLocalizedString("choosePicture", comment: "Choose Picture"), for: .normal)
        if let article = self.articleToEdit {
            self.mytitle.text = article.title
            self.text.text = article.content
            self.imageView.image = UIImage(data: article.image! as Data)
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(clickSave))
    }

    func clickSave() {
        let title = mytitle.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let text = self.text.text.trimmingCharacters(in: .whitespacesAndNewlines)
        let myimage = imageView.image
        
        if (title == "" || text == "" || myimage == nil) {
            return
        }
        if articleToEdit == nil {
            saveArticle(title: title!, text: text, myimage: myimage!)
        }
        else {
            editArticle(title: title!, text: text, myimage: myimage!)
        }
    }
    
    func saveArticle(title: String, text: String, myimage: UIImage) {
        
        let articleManager = ArticleManager(context: context)
        let newArticle = articleManager.newArticle()
        newArticle.title = title
        newArticle.content = text
        newArticle.creation_date = NSDate()
        newArticle.modification_date = newArticle.creation_date
        newArticle.image = UIImagePNGRepresentation(myimage)! as NSData
        newArticle.lang = systemLang
        articleManager.save()
        _ = navigationController?.popViewController(animated: true)
    }
    
    func editArticle(title: String, text: String, myimage: UIImage) {
        let articleManager = ArticleManager(context: context)
        articleToEdit?.title = title
        articleToEdit?.content = text
        articleToEdit?.image = UIImagePNGRepresentation(myimage)! as NSData
        articleToEdit?.modification_date = NSDate()
        articleManager.save()
        articleToEdit = nil
        deleteTempArticle()
        _ = navigationController?.popViewController(animated: true)
    }
    
    func deleteTempArticle() {
        self.articleToEdit = nil
        self.text.text = ""
        self.mytitle.text = ""
        self.imageView.image = nil
    }

    
}

extension AddViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBAction func takePicture(_ sender: UIButton) {
        print("taking picture")
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func choosePicture(_ sender: UIButton) {
        print("choose picture")
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
}
